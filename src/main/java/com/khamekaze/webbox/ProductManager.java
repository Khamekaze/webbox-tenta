
package com.khamekaze.webbox;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the methods of adding products to a List
 * and converting them to usable data.
 * 
 * @author Kim Göransson
 */
public class ProductManager {
    
    
    
    private List<Product> products;
    
    /**
     * A list of products.
     * 
     * Creates an ArrayList of products that are
     * available.
     * 
     * @return ArrayList of available products
     */
    public List<Product> productList() {
        products = new ArrayList<>();
        
        
        Product p1 = new Product();
        p1.setName("The Orange Box");
        p1.setId(1);
        p1.setPrice(999);
        p1.setImage("box9.png");
        
        Product p2 = new Product();
        p2.setName("High class Box");
        p2.setId(2);
        p2.setPrice(350);
        p2.setImage("box8.png");
        
        Product p3 = new Product();
        p3.setName("Cat in a box (cat not included)");
        p3.setId(3);
        p3.setPrice(100);
        p3.setImage("box7.png");
        
        Product p4 = new Product();
        p4.setName("Box in a box in a box in a..");
        p4.setId(4);
        p4.setPrice(800);
        p4.setImage("box6.png");
        
        Product p5 = new Product();
        p5.setName("Mystery box");
        p5.setId(5);
        p5.setPrice(700);
        p5.setImage("box5.png");
        
        Product p6 = new Product();
        p6.setName("Many boxes");
        p6.setId(6);
        p6.setPrice(500);
        p6.setImage("box4.png");
        
        Product p7 = new Product();
        p7.setName("Insane amount of boxes");
        p7.setId(7);
        p7.setPrice(1000);
        p7.setImage("box3.png");
        
        Product p8 = new Product();
        p8.setName("Elite box");
        p8.setId(8);
        p8.setPrice(500);
        p8.setImage("box2.png");
        
        Product p9 = new Product();
        p9.setName("Two boxes");
        p9.setId(9);
        p9.setPrice(200);
        p9.setImage("box1.png");
        
        
        products.add(p1);
        products.add(p2);
        products.add(p3);
        products.add(p4);
        products.add(p5);
        products.add(p6);
        products.add(p7);
        products.add(p8);
        products.add(p9);
        
        
        return products;
    }
    
    /**
     * String in JSON format.
     * 
     * Converts the allProducts() method to a String
     * in the format of a JSON object array
     * 
     * @return a string in JSON array format
     */
    public String JsonList() {
        String json = new Gson().toJson(productList());
        
        return json;
    }
    
}
