package com.khamekaze.webbox;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles the data for the basket.
 * 
 * @author Kim Göransson
 */
public class BasketController extends HttpServlet {
    
    private int id;
    
    /**
     * Sets id to 0.
     * 
     * @throws ServletException 
     */
    @Override
    public void init() throws ServletException {
        id = 0;
        
    }
    
    /**
     * Handles the view of the basket page.
     * 
     * Gets a Basket object from current session.
     * 
     * Responds with the basket ArrayList parsed to a string in
     * JSON object array format.
     * 
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        
        Basket basket = (Basket)session.getAttribute("basket");
        if(basket == null) {
        basket = new Basket();
        }
        
        String list = basket.JsonList();
        
        resp.setContentType("application/json; charset=UTF-8");
        session.setAttribute("basket", basket);
        resp.getWriter().println(list);
    }

    /**
     * Handles addition of products to basket.
     * 
     * Adds a product to basket.
     * Uses the request parameters to set name, price and imagepath
     * of a product to be added in basket.
     * 
     * Adds the product to basket using the addProduct() method.
     * 
     * Uses the current session to store the basket.
     * 
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        
        id++;
        
        Basket basket = (Basket)session.getAttribute("basket");
        if(basket == null) {
        basket = new Basket();
        }
        
        
        
        String name = req.getParameter("name");
        int pId = id;
        String priceString = req.getParameter("price");
        int price = Integer.parseInt(priceString);
        String image = req.getParameter("image");
        
        Product p = new Product();
        p.setName(name);
        p.setId(pId);
        p.setPrice(price);
        p.setImage(image);
        
        basket.addProduct(p);
        
        session.setAttribute("basket", basket);
        getServletContext().getRequestDispatcher("/products.html").forward(req, resp);
        
    }
}
