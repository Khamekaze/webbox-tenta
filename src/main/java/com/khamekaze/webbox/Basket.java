package com.khamekaze.webbox;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;



/**
 * Handles product storage.
 * 
 * Responsible for returning a List of products.
 * Contains methods for adding, removing and parsing
 * an ArrayList of products.
 * 
 * @author Kim Göransson
 */
public class Basket {
    
    private List<Product> basket = new ArrayList<>();
    
    
    /**
     * Adds a product to the ArrayList.
     * 
     * Recieves a product and adds it to the
     * ArrayList.
     * 
     * @param product object to be added
     * @return a modified ArrayList
     */
    public List<Product> addProduct(Product product) {
        
        basket.add(product);
        
        return basket;
    }
    
    
    /**
     * An ArrayList of current products in basket
     * 
     * @return an ArrayList of products
     */
    public List<Product> allProducts() {
        return basket;
    }
    
    /**
     * String in JSON format.
     * 
     * Converts the allProducts() method to a String
     * in the format of a JSON object array
     * 
     * @return a string in JSON array format
     */
    public String JsonList() {
        String json = new Gson().toJson(allProducts());
        return json;
    }
    
    
    /**
     * Removes specific product from ArrayList.
     * 
     * Recieves an id and checks if the corresponding
     * product exists in the allProducts() method.
     * 
     * If the product is found, it removed from the ArrayList.
     * 
     * The modified ArrayList is then returned.
     * 
     * @param id the id to be searched for
     * @return a modified ArrayList
     */
    public List<Product> removeProduct(int id) {
        
        for(int i = 0; i < allProducts().size(); i++) {
            Product p = allProducts().get(i);
            if(id == p.getId()) {
                allProducts().remove(i);
            }
        }
        return allProducts();
    }
    
}
