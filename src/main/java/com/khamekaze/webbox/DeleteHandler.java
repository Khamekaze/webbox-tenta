package com.khamekaze.webbox;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles the removal of products from the basket.
 * 
 *
 * @author Kim Göransson
 */
public class DeleteHandler extends HttpServlet {
    
    /**
     * Removes a product from the basket.
     * 
     * Recieves an id parameter and run the removeProduct()
     * method.
     * 
     * 
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException 
     */
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        HttpSession session = req.getSession();
        
        Basket basket = (Basket)session.getAttribute("basket");
        
        String idString = req.getParameter("id");
        int id = Integer.parseInt(idString);
        
        basket.removeProduct(id);
        
        session.setAttribute("basket", basket);
        
        getServletContext().getRequestDispatcher("/basket.jsp").forward(req, resp);
    }
    
}
