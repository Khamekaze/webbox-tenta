$(function() {
    getAjax();
    $("#search input[type = button]").click(getAjax);
});
                                            
function getAjax(){
        $.getJSON('products.json', {}, ajaxResult);
}

function ajaxResult(result){
    var textInput = $("#search input[type = text]").val();
    var maxPrice;
    var price = $("#search input[type = radio]");
    for(var i = 0; i < price.length; i++){
        var radio = $(price[i]);
        if(radio.prop("checked")){
            maxPrice = radio.prop("value");
        }
        
    }
    
    var store = $("#content_bottom");
    store.empty();
    for(var i = 0; i < result.products.length; i++){
        var product = result.products[i];
      
        var item = $('<div/>', {"class": "products_bottom_block", "id": "products_bottom_block-" + i});
        var inner = $('<div/>',  {"class": "block_content", "id": "block_content-" + i});
        
        var image = $("<img/>", {"class": "image", "src": product.image, "alt": "Image of " + product.name});
        var title = $('<span/>', {"class": "name"}).html(product.name).append(product.price);
        
        var formStart = $("<form method=\"post\" action=\"/WebBox/BasketController\">");
        var hidden = $("<input type=\"hidden\" name=\"name\" value=\"" + product.name + "\"/>" +
            "<input type=\"hidden\" name=\"id\" value=\"" + product.id + "\"/>" +
            "<input type=\"hidden\" name=\"price\" value=\"" + product.price + "\"/>" + 
            "<input type=\"hidden\" name=\"image\" value=\"" + product.image + "\"/>" + 
            "<input type=\"submit\" value=\"Add to basket\"/>" + "</form>");
        
        item.append(inner);
        inner.append(image);
        inner.append(title);
        formStart.append(hidden);
        inner.append(formStart);
        
        store.append(item);
    }
}