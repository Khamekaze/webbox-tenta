<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Webbox</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28/angular.min.js"></script>
<script src="jquery-1.11.1.js"></script>
<script src="javascript.js"></script>

</head>

<body ng-app="" ng-controller="customersController">
	<div id = "header">
    
    <div id = "logo">
    </div>
        
    <div id = "menu_container">
         <ul id="sddm">
             
             <li>
    <a href="index.html"
       onmouseover="mopen('m1')"
       onmouseout="mclosetime()">HOME</a>
        <div id="m1"
             onmouseover="mcancelclosetime()"
             onmouseout="mclosetime()">
        </div>
    </li>
    <li><a href="#" 
        onmouseover="mopen('m2')" 
        onmouseout="mclosetime()">PRODUCTS</a>
        <div id="m2" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
        <a href="products.html">Show All</a>
        <a href="#">Ultimate Box Bundle</a>
        </div>
    </li>
    <li><a href="about.html" 
        onmouseover="mopen('m3')"
        onmouseout="mclosetime()">ABOUT</a>
    
    </li>
             <li>
        <a href="basket.jsp"
           onmouseover="mopen('m1')"
           onmouseout="mclosetime()">BASKET ({{items.length}})</a>         
    </li>
             
    
</ul>
<div style="clear:both"></div>
    </div>
</div>
    <div id = "content_container">
        <div id = "basket_top">
        </div>
    <div id = "basket_container">
        
            <div class="cart_item_block" ng-repeat="item in items">
                
                <div class="cart_item_image">
                    <img ng-src="{{item.image}}"/>
                </div>
                
                <div class="cart_item_description">
                    {{item.name}} {{item.price}}
                </div>
                <div class="cart_item_delete">
                    <form method="post" action="/WebBox/DeleteHandler">
                        <input type="hidden" name="id" value="{{item.id}}"/>
                        <input type="submit" class="delete_button" value=""/>
                    </form>
                    
                </div>
                
            </div>
        <div id = "checkout_container">
        Total price: {{getTotal()}}
        </div> 
        </div>
         
        
    </div>    
        
    
    
    <script>
        function customersController($scope,$http) {
          $http.get("/WebBox/BasketController")
          .success(function(response) {$scope.items = response; console.log(response);}).error(function(e){console.log(e);});
        
            $scope.getTotal = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
            var item = $scope.items[i];
            var amount = $scope.items.length;
            total = (item.price * amount);
             }
        return total;
        };
    };
        
    </script>
    
</body>
</html>
